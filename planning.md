**Feature 1**
1. git clone url
2. cd new directory
3. python -m venv .venv
4. source .venv/bin/activate
5. pip install django
6. python -m pip install --upgrade pip
7. pip install black
8. pip install flake8
9. pip install djhtml
10. deactivate
11. source .venv/bin/activate
12. pip freeze > requirements.txt

**Feature 2**
1. django-admin startproject tracker .
2. python manage.py startapp accounts
3. python manage.py startapp projects
4. python manage.py startapp tasks
5. python manage.py makemigrations
6. python manage.py migrate
7. python manage.py createsuperuser

**Feature 3**
in project models.py

`USER_MODEL = settings.AUTH_USER_MODEL`

`class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(USER_MODEL, related_name="projects")`

    def __str__(self):
        return self.name
